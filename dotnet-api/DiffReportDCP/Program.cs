﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DeltaXML.CoreS9Api;
using com.deltaxml.api;

namespace DiffReportDCP
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                DirectoryInfo samplesFolder = locateSamplesDirectory("DCPdiffReport");

                FileInfo input1 = new FileInfo(Path.Combine(samplesFolder.FullName, "input1.xml"));
                FileInfo input2 = new FileInfo(Path.Combine(samplesFolder.FullName, "input2.xml"));
                DirectoryInfo resultFolder = samplesFolder.CreateSubdirectory("result");
                FileInfo dcpDefinition = new FileInfo(Path.Combine(samplesFolder.FullName, "diffreport.dcp"));
                DirectoryInfo dcResultFolder = resultFolder.CreateSubdirectory("DCP_API");

                Dictionary<String, String> dcpStringParams = new Dictionary<String, String>();
                dcpStringParams.Add("formatting-element-list", "b,i,u");
                Dictionary<String, Boolean> dcpBooleanParams = new Dictionary<String, Boolean>();
                dcpBooleanParams.Add("convert-to-html", true);

                Console.WriteLine("Initialising configuration");
                DCPConfiguration dcp = new DCPConfiguration(dcpDefinition);

                Console.WriteLine("Generating new DocumentComparator with configuration 1");
                dcp.generate(dcpBooleanParams, dcpStringParams);

                DocumentComparator comparator = dcp.DocumentComparator;

                Console.WriteLine("Starting comparison");
                FileInfo result1 = new FileInfo(Path.Combine(dcResultFolder.FullName, "result1.html"));
                comparator.compare(input1, input2, result1);
                Console.WriteLine("Comparison complete. Result 1 output to: " + result1.FullName);

                Console.WriteLine("Modifying DocumentComparator for configuration 2");
                dcpBooleanParams["convert-to-html"] = false;
                dcp.setParams(dcpBooleanParams, dcpStringParams);

                Console.WriteLine("Starting comparison");
                FileInfo result2 = new FileInfo(Path.Combine(dcResultFolder.FullName, "result2.xml"));
                comparator.compare(input1, input2, result2);
                Console.WriteLine("Comparison complete. Result 2 output to: " + result2.FullName);
                Console.WriteLine("--------------COMPLETED---------------");
            }
            catch (DeltaXMLException e)
            {
                Console.WriteLine("Error: " + e.getMessage());
            }
            finally
            {
                Console.WriteLine("Press any key...");
                ConsoleKeyInfo response = Console.ReadKey();
            }
        }

        private static DirectoryInfo locateSamplesDirectory(String sampleDir)
        {
            DirectoryInfo docdir = new DirectoryInfo(".");
            while (docdir.Exists && docdir.Name != "samples")
            {
                docdir = docdir.Parent;
            }
            docdir = new DirectoryInfo(Path.Combine(docdir.FullName, sampleDir));
            return docdir;
        }
    }
}
