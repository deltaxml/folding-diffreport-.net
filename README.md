# Folding DiffReport

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/folding-diffreport`.*

---

A demonstration of the capabilities of the folding DiffReport output format.

This document describes how to run the sample. For concept details see: [Folding DiffReport with DCP](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/folding-diffreport-with-dcp)

## Running with the Command-Line Interface

To run the sample using the command-line interface for the deltaxml executable, launch the batch file run-dcp.bat, either from the command-line or from Windows Explorer. Alternatively, run with the sample DCP directly from the command-line with the following command:

	..\..\bin\deltaxml.exe compare dcp-folding input1.xml input2.xml result.html
	
## Running with C# and the .NET API DCPConfiguration class

From Visual Studio, select File, Open Project/Solution from the menu-bar (Ctrl-Shift-O), navigate to the *dotnet-api* directory and open the DiffReportDCP.sln file. Once the DiffReportDCP project is loaded, you can build and run it in the usual way for Visual Studio (keyboard shortcut: F5).

# **Note - .NET support has been deprecated as of version 10.0.0 **