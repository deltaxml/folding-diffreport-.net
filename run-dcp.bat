set DXL_BIN=..\..\bin
set FORMATTING_ELEMENTS=b,i,u
mkdir result
mkdir result\DCP
%DXL_BIN%\deltaxml.exe compare dcp-folding input1.xml input2.xml result\DCP\output.html convert-to-html=true formatting-element-list=%FORMATTING_ELEMENTS%
%DXL_BIN%\deltaxml.exe compare dcp-folding input1.xml input2.xml result\DCP\output.xml convert-to-html=false formatting-element-list=%FORMATTING_ELEMENTS%
start result\DCP\output.html